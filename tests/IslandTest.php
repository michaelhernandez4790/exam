<?php 

use PHPUnit\Framework\TestCase;

final class IslandTest extends TestCase
{
    public function testSearchIsland(): void
    {
        $island = new Island();
        $this->assertInstanceOf(Island::class, $island);

        $map = [
            [0,1,0],
            [1,1,1],
        ];
        $this->assertEquals(4, $island->search($map));

        $map = [
            [1,0,0,0,1],
            [0,0,0,0,1],
            [1,1,1,0,0],
            [1,0,0,0,0],
            [1,0,0,0,0],
        ];
        $this->assertEquals(5, $island->search($map));

        $map = [
            [1,0,0,0,1],
            [0,1,0,0,1],
            [1,1,1,0,1],
        ];
        $this->assertEquals(4, $island->search($map));

        $map = [
            [1,0,0,1,1],
            [0,1,0,0,1],
            [1,1,1,0,1],
        ];
        $this->assertEquals(4, $island->search($map));
    }

    public function testFailSeachIsland(): void
    {
        $island = new Island();
        $this->assertInstanceOf(Island::class, $island);

        $map = [
            [1,0,0,0,1],
            [0,1,0,0,1],
            [1,1,1,0,0],
            [1,1,1,0,0],
            [1,1,1,0,0],
        ];
        $this->assertNotEquals(9, $island->search($map));

        $map = [
            [1,0,1],
            [0,1,1],
            [1,1,1],
        ];
        $this->assertNotEquals(7, $island->search($map));

        $map = [
            [1,0,1],
        ];
        $this->assertNotEquals(2, $island->search($map));

        $map = [
            ['ERROR',0,1],
        ];
        $this->assertEquals(1, $island->search($map));
    }
}