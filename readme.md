# Requerimientos

- Php
- Composer

## Install

Descargar el repositorio e ir por consola a la carpeta

``
cd /my-folder-application
``

Instalar las dependencias con composer:
``
composer install
``


Ejecutar las pruebas:
``
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/IslandTest
``
