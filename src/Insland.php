<?php 

final class Island
{
    protected $_array;

    public function search(array $array)
    {
        $this->_array = filter_var($array, FILTER_VALIDATE_INT, [
           'flags'   => FILTER_REQUIRE_ARRAY,
           'options' => [
                'min_range' => 0, 
                'max_range' => 1
            ]
        ]);

        $cant = count($this->_array);
        $groups = [];
        
        foreach ($this->_array as $y => $row) {
            foreach ($row as $x => $value) {
                if ($value == 0) {
                    continue;
                }

                $groups[] = $this->route($y, $x);
            }
        }
        return max($groups);
    }

    // recorriendo los 1s
    private function route($y, $x, $dir = 'top', $sum = 1)
    {
        $sum = empty($sum) ? 1 : $sum;
        
        // buscando hacia la izquierda (no se ejecuta si viene desde la derecha por recursion infinita)
        if (!empty($this->_array[$y][$x - 1]) && $dir != 'der') {
            $sum++;
            $sum = $this->route($y, $x - 1, 'izq', $sum);
            $this->_array[$y][$x - 1] = 0;
        }

        // buscando hacia la derecha (no se ejecuta si viene desde la izquierda por recursion infinita)
        if (!empty($this->_array[$y][$x + 1]) && $dir != 'izq') {
            $sum++;
            $sum = $this->route($y, $x + 1, 'der', $sum);
            $this->_array[$y][$x + 1] = 0;
        }

        // buscando hacia abajo
        if (!empty($this->_array[$y + 1][$x])) {
            $sum++;
            $sum = $this->route($y + 1, $x, 'bottom', $sum);
            $this->_array[$y + 1][$x] = 0;
        }

        return $sum;
    }
}